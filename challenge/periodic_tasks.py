# coding=utf-8
from celery.schedules import crontab

CELERY_PERIODIC_TASKS = {
    'challenge-every-minute': {
        'task': 'applications.accounts.tasks.run_accounts',
        'schedule': crontab(minute='*/1')
    },
}
