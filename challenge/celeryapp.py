# coding=utf-8
from __future__ import absolute_import

import os

from celery import Celery

from challenge.periodic_tasks import CELERY_PERIODIC_TASKS

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'challenge.settings')
app = Celery('challenge')
app.config_from_object('django.conf:settings')
from django.apps import apps

app.autodiscover_tasks(lambda: [n.name for n in apps.get_app_configs()])

app.conf.update(CELERYBEAT_SCHEDULE=CELERY_PERIODIC_TASKS)
