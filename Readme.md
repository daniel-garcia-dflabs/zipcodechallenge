# Installation

## Requirements
* Install pip
* Install Python 3.7


## Git

**Clone the project**

`$ git clone https://gitlab.com/daniel-garcia-dflabs/zipcodechallenge`


## Setup 

**Create environment** 


`$ cd zipcodechallenge/`

`$ python3.7 -m venv env`

> New python executable in env/bin/python 

> Installing distribute..............done. 

> Installing pip.....................done. 

`$ source env/bin/activate`

> (env)user@system:~$  


**Install requirements** 

`(env)$ pip install -r requirements.txt`

> Downloading/unpacking django(...) 

> Installing collected packages: django(...) 

> Successfully installed django

> Cleaning up... 

Run migrations

`(env)$ python manage.py makemigrations accounts`

`(env)$ python manage.py migrate`

## Run Tests

`(env)$ python manage.py test applications.accounts`

## Schedule task

`(env)$ env/bin/celery --app=challenge.celeryapp:app beat -l info --config=celeryconfig`