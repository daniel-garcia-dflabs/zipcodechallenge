# Create your tests here.
from datetime import timedelta

from django.test import TestCase
from django.utils.timezone import now

from applications.accounts.models import Account
from applications.accounts.tasks import get_account_ids_to_run


class AccountsTest(TestCase):

    def setUp(self):
        # Create 30 accounts
        self.moment = now()

        for i in range(0, 30):
            Account.objects.create(id=i)

        # Simulate 10 accounts updated 16 minutes ago
        for i in range(0, 10):
            account = Account.objects.get(pk=i)
            account.date_last_run = self.moment - timedelta(minutes=16)
            account.save()

        # Simulate 105 accounts updated 15 minutes ago
        for i in range(10, 25):
            account = Account.objects.get(pk=i)
            account.date_last_run = self.moment - timedelta(minutes=15)
            account.save()

        # Simulate 5 accounts updated 14 minutes ago
        for i in range(25, 30):
            account = Account.objects.get(pk=i)
            account.date_last_run = self.moment - timedelta(minutes=14)
            account.save()

    def test_minute_per_minute_run(self):
        # Running now, will evaluate accounts updated from 16 - 15 minutes ago
        first_run  = len(get_account_ids_to_run(Account.objects.all(), self.moment))
        self.assertEqual(first_run, 10)

        # Running now, will evaluate accounts updated from 16 - 15 minutes ago starting from now +  1 minute
        first_run = len(get_account_ids_to_run(Account.objects.all(), self.moment + timedelta(minutes=1)))
        self.assertEqual(first_run, 15)

        # Running now, will evaluate accounts updated from 16 - 15 minutes ago starting from now +  2 minute
        first_run = len(get_account_ids_to_run(Account.objects.all(), self.moment + timedelta(minutes=2)))
        self.assertEqual(first_run, 5)
