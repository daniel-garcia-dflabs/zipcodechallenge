from datetime import timedelta

from django.db import transaction
from django.utils.timezone import now

from applications.accounts.models import Account
from challenge.celeryapp import app


def get_account_ids_to_run(account_ids_list=None, moment=now()):
    ago_end = moment - timedelta(minutes=15)
    ago_start = ago_end - timedelta(minutes=1)
    if account_ids_list is None:
        account_ids_list = []
    accounts = Account.objects.filter(id__in=account_ids_list,
                                      date_last_run__gte=ago_start,
                                      date_last_run__lt=ago_end)

    return accounts.values_list('id', flat=True)


@app.task
def run_accounts():
    try:
        with transaction.atomic():
            moment = now()
            ids_to_run = get_account_ids_to_run(account_ids_list=Account.objects.all().values_list('id', flat=True),
                                                moment=moment)
            """
            Run transaction goes here
            """
            Account.objects.filter(id__in=ids_to_run).update(**{'date_last_run': moment})
            print(ids_to_run)
    except Exception as e:
        print(e)
