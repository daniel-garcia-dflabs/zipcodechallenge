from django.db import models


# Create your models here.
class Account(models.Model):
    name = models.CharField(max_length=500)
    date_last_run = models.DateTimeField(auto_now_add=True)
